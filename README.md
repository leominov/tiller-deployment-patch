# Tiller Deployment Patch

6 августа замечены поды Tiller'а от Helm 2 в статусе `ImagePullBackOff`, как оказалось, образы больше недоступны в Google Container Registry:

```
% docker pull gcr.io/kubernetes-helm/tiller:v2.15.2
Error response from daemon: manifest for gcr.io/kubernetes-helm/tiller:v2.15.2 not found: manifest unknown: Failed to fetch "v2.15.2" from request "/v2/kubernetes-helm/tiller/manifests/v2.15.2".
```

Патч заменяет используемый образ на `eu.gcr.io/qlean-242314/gitlab/platform/infra/tiller:v2.15.2` во всех деплойментах `tiller-deploy` по селектору `name=tiller`.

## Ссылки

* https://qlean.slack.com/archives/C06PG9WBB/p1628252816086300
* https://qlean.slack.com/archives/C06PG9WBB/p1628253506089300
