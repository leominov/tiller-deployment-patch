#!/bin/bash

namespace="$1"

if [[ -z "${namespace}" ]]; then
	namespaces=$(kubectl get deploy -l name=tiller --all-namespaces --no-headers -o custom-columns=NAME:.metadata.namespace)
	for ns in ${namespaces[*]}; do
		echo "===> ${ns}"
		kubectl -n "${ns}" patch deploy/tiller-deploy --patch "$(cat tiller-patch.yaml)"
	done
else
	kubectl -n "${namespace}" patch deploy/tiller-deploy --patch "$(cat tiller-patch.yaml)"
fi
